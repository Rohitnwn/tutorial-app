var app = angular.module("studentApp", ['ngRoute']);

app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "html/login.html",
        controller : "loginCtrl"
    })
    .when("/search", {
        templateUrl : "html/search.html",
        controller : "searchCtrl"
    })
    .when("/results", {
        templateUrl : "html/results.html",
        controller : "resultCtrl"
    })
});






