data = {
    users: [
        { email: "alpha@test.com", password: "@A123456" },
        { email: "beta@test.com", password: "@A654321" }

    ],

    gradeBoard: [
        { id: 1, grade: 6, board: "cbse" },
        { id: 2, grade: 6, board: "icse" },
        { id: 3, grade: 7, board: "cbse" },
        { id: 4, grade: 7, board: "icse" },
        { id: 5, grade: 8, board: "cbse" },
        { id: 6, grade: 8, board: "icse" },
        { id: 7, grade: 9, board: "cbse" },
        { id: 8, grade: 9, board: "icse" },
        { id: 9, grade: 10, board: "cbse" },
        { id: 10, grade: 10, board: "icse" },
        { id: 11, grade: 11, board: "cbse" },
        { id: 12, grade: 11, board: "icse" },
        { id: 13, grade: 12, board: "cbse" },
        { id: 14, grade: 12, board: "icse" }
    ],

    teststable: [
        { id: 1, gradeBoardId: [1, 2], subject: "Chemistry", duration: "30 mins" },
        { id: 2, gradeBoardId: [1, 2], subject: "Physics", duration: "30 mins" },
        { id: 3, gradeBoardId: [1, 2], subject: "Maths", duration: "30 mins" },
        { id: 4, gradeBoardId: [3, 4], subject: "Chemistry", duration: "30 mins" },
        { id: 5, gradeBoardId: [3, 4], subject: "Physics", duration: "30 mins" },
        { id: 6, gradeBoardId: [3, 4], subject: "Maths", duration: "30 mins" },
        { id: 7, gradeBoardId: [5], subject: "Chemistry", duration: "30 mins" },
        { id: 8, gradeBoardId: [5], subject: "Physics", duration: "30 mins" },
        { id: 9, gradeBoardId: [5], subject: "Maths", duration: "30 mins" },
        { id: 10, gradeBoardId: [6], subject: "Chemistry", duration: "30 mins" },
        { id: 11, gradeBoardId: [6], subject: "Physics", duration: "30 mins" },
        { id: 12, gradeBoardId: [6], subject: "Maths", duration: "30 mins" },
        { id: 13, gradeBoardId: [7], subject: "Chemistry", duration: "30 mins" },
        { id: 14, gradeBoardId: [7], subject: "Physics", duration: "30 mins" },
        { id: 15, gradeBoardId: [7], subject: "Maths", duration: "30 mins" },
        { id: 16, gradeBoardId: [8], subject: "Chemistry", duration: "30 mins" },
        { id: 17, gradeBoardId: [8], subject: "Physics", duration: "30 mins" },
        { id: 18, gradeBoardId: [8], subject: "Maths", duration: "30 mins" },
        { id: 19, gradeBoardId: [9], subject: "Chemistry", duration: "30 mins" },
        { id: 20, gradeBoardId: [9], subject: "Physics", duration: "30 mins" },
        { id: 21, gradeBoardId: [9], subject: "Maths", duration: "30 mins" },
        { id: 22, gradeBoardId: [10, 11], subject: "Chemistry", duration: "30 mins" },
        { id: 23, gradeBoardId: [10, 11], subject: "Physics", duration: "30 mins" },
        { id: 24, gradeBoardId: [10, 11], subject: "Maths", duration: "30 mins" },
        { id: 25, gradeBoardId: [12], subject: "Chemistry", duration: "30 mins" },
        { id: 26, gradeBoardId: [12], subject: "Physics", duration: "30 mins" },
        { id: 27, gradeBoardId: [12], subject: "Maths", duration: "30 mins" }
    ],

    docstable: [
        { id: 1, gradeBoardId: [1, 2], subject: "Chemistry", submited_by: "XYZ" },
        { id: 2, gradeBoardId: [1, 2], subject: "Physics", submited_by: "XYZ" },
        { id: 3, gradeBoardId: [1, 2], subject: "Maths", submited_by: "XYZ" },
        { id: 4, gradeBoardId: [3, 4], subject: "Chemistry", submited_by: "XYZ" },
        { id: 5, gradeBoardId: [3, 4], subject: "Physics", submited_by: "XYZ" },
        { id: 6, gradeBoardId: [3, 4], subject: "Maths", submited_by: "XYZ" },
        { id: 7, gradeBoardId: [5], subject: "Chemistry", submited_by: "XYZ" },
        { id: 8, gradeBoardId: [5], subject: "Physics", submited_by: "XYZ" },
        { id: 9, gradeBoardId: [5], subject: "Maths", submited_by: "XYZ" },
        { id: 10, gradeBoardId: [6], subject: "Chemistry", submited_by: "XYZ" },
        { id: 11, gradeBoardId: [6], subject: "Physics", submited_by: "XYZ" },
        { id: 12, gradeBoardId: [6], subject: "Maths", submited_by: "XYZ" },
        { id: 13, gradeBoardId: [7], subject: "Chemistry", submited_by: "XYZ" },
        { id: 14, gradeBoardId: [7], subject: "Physics", submited_by: "XYZ" },
        { id: 15, gradeBoardId: [7], subject: "Maths", submited_by: "XYZ" },
        { id: 16, gradeBoardId: [8], subject: "Chemistry", submited_by: "XYZ" },
        { id: 17, gradeBoardId: [8], subject: "Physics", submited_by: "XYZ" },
        { id: 18, gradeBoardId: [8], subject: "Maths", submited_by: "XYZ" },
        { id: 19, gradeBoardId: [9], subject: "Chemistry", submited_by: "XYZ" },
        { id: 20, gradeBoardId: [9], subject: "Physics", submited_by: "XYZ" },
        { id: 21, gradeBoardId: [9], subject: "Maths", submited_by: "XYZ" },
        { id: 22, gradeBoardId: [10, 11], subject: "Chemistry", submited_by: "XYZ" },
        { id: 23, gradeBoardId: [10, 11], subject: "Physics", submited_by: "XYZ" },
        { id: 24, gradeBoardId: [10, 11], subject: "Maths", submited_by: "XYZ" },
        { id: 25, gradeBoardId: [12], subject: "Chemistry", submited_by: "XYZ" },
        { id: 26, gradeBoardId: [12], subject: "Physics", submited_by: "XYZ" },
        { id: 27, gradeBoardId: [12], subject: "Maths", submited_by: "XYZ" }
    ],

    assignmentstable: [
        { id: 1, gradeBoardId: [1, 2], subject: "Chemistry", topic: "periodic table" },
        { id: 2, gradeBoardId: [1, 2], subject: "Physics", topic: "periodic table" },
        { id: 3, gradeBoardId: [1, 2], subject: "Maths", topic: "periodic table" },
        { id: 4, gradeBoardId: [3, 4], subject: "Chemistry", topic: "periodic table" },
        { id: 5, gradeBoardId: [3, 4], subject: "Physics", topic: "periodic table" },
        { id: 6, gradeBoardId: [3, 4], subject: "Maths", topic: "periodic table" },
        { id: 7, gradeBoardId: [5], subject: "Chemistry", topic: "periodic table" },
        { id: 8, gradeBoardId: [5], subject: "Physics", topic: "periodic table" },
        { id: 9, gradeBoardId: [5], subject: "Maths", topic: "periodic table" },
        { id: 10, gradeBoardId: [6], subject: "Chemistry", topic: "periodic table" },
        { id: 11, gradeBoardId: [6], subject: "Physics", topic: "periodic table" },
        { id: 12, gradeBoardId: [6], subject: "Maths", topic: "periodic table" },
        { id: 13, gradeBoardId: [7], subject: "Chemistry", topic: "periodic table" },
        { id: 14, gradeBoardId: [7], subject: "Physics", topic: "periodic table" },
        { id: 15, gradeBoardId: [7], subject: "Maths", topic: "periodic table" },
        { id: 16, gradeBoardId: [8], subject: "Chemistry", topic: "periodic table" },
        { id: 17, gradeBoardId: [8], subject: "Physics", topic: "periodic table" },
        { id: 18, gradeBoardId: [8], subject: "Maths", topic: "periodic table" },
        { id: 19, gradeBoardId: [9], subject: "Chemistry", topic: "periodic table" },
        { id: 20, gradeBoardId: [9], subject: "Physics", topic: "periodic table" },
        { id: 21, gradeBoardId: [9], subject: "Maths", topic: "periodic table" },
        { id: 22, gradeBoardId: [10, 11], subject: "Chemistry", topic: "periodic table" },
        { id: 23, gradeBoardId: [10, 11], subject: "Physics", topic: "periodic table" },
        { id: 24, gradeBoardId: [10, 11], subject: "Maths", topic: "periodic table" },
        { id: 25, gradeBoardId: [12], subject: "Chemistry", topic: "periodic table" },
        { id: 26, gradeBoardId: [12], subject: "Physics", topic: "periodic table" },
        { id: 27, gradeBoardId: [12], subject: "Maths", topic: "periodic table" }
    ]
}

app.constant('Data', data);
app.run(['storageService', 'Data', function(storageService, Data) {
    var users = storageService.getItemLocalStorage("Users")
    if (!users) {
        var users_list = Data.users
        storageService.setItemLocalStorage("Users", users_list)
    }
}]);