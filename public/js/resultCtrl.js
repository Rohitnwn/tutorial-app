app.controller("resultCtrl", function($scope, $location, storageService, Data) {
    $scope.resultPage = {}
    setResults()


    function setResults() {
      var results = storageService.getItemLocalStorage("results");
      if(results){
        $scope.resultPage.tests = results.tests;
        $scope.resultPage.docs = results.docs
        $scope.resultPage.assignments = results.assignments
      }
    }
});