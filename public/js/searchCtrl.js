app.controller("searchCtrl", function($scope, $location, storageService, Data) {
    $scope.searchPage = {
        availableGrades: [6, 7, 8, 9, 10, 11, 12],
        boardInvalid: false,
        passwordInvalid: false,
        resultsInvalid: false
    };

    function validateSearchForm() {
        $scope.searchPage.gradeInvalid = false;
        $scope.searchPage.boardInvalid = false;

        if (!$scope.searchPage.board) {
            $scope.searchPage.boardInvalid = true;
            $scope.searchPage.boardErrMsg = "Please select Board";
        }
        if (!$scope.searchPage.grade) {
            $scope.searchPage.gradeInvalid = true;
            $scope.searchPage.gradeErrMsg = "Please select Grade";
        }

        return !($scope.searchPage.gradeInvalid || $scope.searchPage.boardInvalid);
    }

    function getRecordById(data, gradeboardId) {
        var res = []
        angular.forEach(data, function(rec) {
            if (rec.gradeBoardId.indexOf(gradeboardId) > -1) {
                res.push(rec)
            }
        });
        return res;
    }

    function getGradeBoardId() {
        var gradeboardId;
        angular.forEach(Data.gradeBoard, function(gb) {
            if (gb.board === $scope.searchPage.board && gb.grade.toString() === $scope.searchPage.grade) {
                gradeboardId = gb.id
            }
        });
        return gradeboardId;
    }

    $scope.searchPage.submit = function() {
        var isValid = validateSearchForm();
        var results = { tests: [], docs: [], assignments: [] };
        if (isValid) {
           $scope.searchPage.resultsInvalid = false;
            var gradeboardId = getGradeBoardId();
            if (gradeboardId) {
                results.tests = getRecordById(Data.teststable, gradeboardId);
                results.docs = getRecordById(Data.docstable, gradeboardId);
                results.assignments = getRecordById(Data.assignmentstable, gradeboardId);
            }
            if (!gradeboardId ||
                  (results.tests.length < 1 &&
                    results.docs.length < 1 &&
                    results.assignments.length < 1
                )
            ) {
                $scope.searchPage.resultsInvalid = true;
                $scope.searchPage.resultsErrMsg = "No tests, doc and assignments avaiable for this selection";
            } else {
                storageService.setItemLocalStorage("results", results);
                $location.path("/results");
            }
        }
    }

});