app.controller("loginCtrl", function($scope, $location, storageService) {
   $scope.loginPage = {
      emailInvalid: false,
      passwordInvalid: false
   }

   function findUserInDB (curr_user){
      users = storageService.getItemLocalStorage("Users")
      res = null
      angular.forEach(users, function(user){
        if(user.email === curr_user){
          res = user
        }
      });
      return res
   }

   $scope.loginPage.loginUser = function(){
      $scope.loginPage.validate_email()
      $scope.loginPage.validate_password()

      if(!$scope.loginPage.emailInvalid && !$scope.loginPage.passwordInvalid){
        user = findUserInDB($scope.loginPage.email)
        if(!user){
          $scope.loginPage.emailInvalid = true;
          $scope.loginPage.emailErrMsg = "User not found with this email id"
        }else if(user.password === $scope.loginPage.password){
          $location.path("/search")
        }else if(user.password !== $scope.loginPage.password){
          $scope.loginPage.passwordInvalid = true;
          $scope.loginPage.passswordErrMsg = "Incorrect password"
        }
      }
   }

   $scope.loginPage.validate_email = function(){
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if(!$scope.loginPage.email){
        $scope.loginPage.emailInvalid = true;
        $scope.loginPage.emailErrMsg = "Email is required"
      }else if (!re.test($scope.loginPage.email)) {
        $scope.loginPage.emailInvalid = true;
        $scope.loginPage.emailErrMsg = "Invalid Email"
      }else{
        $scope.loginPage.emailInvalid = false
      }
   }

   $scope.loginPage.validate_password = function(){
      re = new RegExp("^(?=.*[A-Z])(?=.*[!@#\$%\^&\*])(?=.{8,})")
      if(!$scope.loginPage.password){
        $scope.loginPage.passwordInvalid = true;
        $scope.loginPage.passswordErrMsg = "Password is required"
      }else if (!re.test($scope.loginPage.password)) {
        $scope.loginPage.passwordInvalid = true;
        $scope.loginPage.passswordErrMsg = "Invalid password. It must contain one capital and special character"
      }else {
        $scope.loginPage.passwordInvalid = false
      }
   }
});
