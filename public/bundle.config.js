module.exports = {
  bundle: {
    main: {
      scripts: [
        './public/js/core.js',
        './public/js/data.js'
      ],
      styles: './public/**/*.css'
    },
    vendor: {
      scripts: './node_modules/angular/angular.js'
    }
  },
  copy: './content/**/*.{png,svg}'
};