### What is this repository for? ###

* This is Tutorail application in which user can login and browser test, assignments, docs for grade and board.

### How do I get set up? ###

* install node(7.4.0), gulp(3.9.1), npm(4.0.5)
* npm install (will install all project dependencies)
* exec 'node server.js' to run application on localhost:8080

