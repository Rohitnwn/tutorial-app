//gulp
var gulp = require('gulp');
// plugins
var uglify = require('gulp-uglify');
var minifyCSS = require('gulp-minify-css');
var clean = require('gulp-clean');
var runSequence = require('run-sequence');
var concat = require('gulp-concat');
var include = require('gulp-include')
var bundle = require('gulp-bundle-assets');


// gulp.task('bundle', function() {
//   gulp.src('./bundle.config.js')
//     .pipe(bundle())
//     .pipe(bundle.results('./'))
//     .pipe(gulp.dest('./dist'));
// });

// tasks
gulp.task('clean', function() {
    gulp.src('./dist/*')
      .pipe(clean({force: true}));
});

gulp.task('minify-css', function() {
  var opts = {comments:true,spare:true};
  gulp.src(['./public/**/*.css'])
    .pipe(minifyCSS(opts))
    .pipe(concat('main.css'))
    .pipe(gulp.dest('./dist/'))
});

gulp.task('minify-js', function() {
  gulp.src(['./public/**/*.js'])
    .pipe(uglify({
      // inSourceMap:
      // outSourceMap: "app.js.map"
    }))
    .pipe(include({
      extensions: "js",
      hardFail: true,
      includePaths: [
         "/node_modules/**"
      ]
    }))
    .pipe(concat('main.js'))
    .pipe(gulp.dest('./dist/'))
});

gulp.task('copy-html-files', function () {
  gulp.src('./public/**/*.html')
    .pipe(gulp.dest('dist/'));
});

gulp.task('build', function() {
  runSequence(
    ['clean'],
    ['minify-css', 'minify-js', 'copy-html-files']
  );
});